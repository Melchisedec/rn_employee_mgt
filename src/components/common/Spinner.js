import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = () => {
    const { SpinnerStyle } = styles;
    return (
        <View style={SpinnerStyle}>
            <ActivityIndicator />
        </View>
    );
};

const styles = {
    SpinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export { Spinner };
